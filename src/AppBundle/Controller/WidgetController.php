<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/widget")
 */

class WidgetController extends Controller
{
    /**
     * @Route("/categories", name="widget_categories")
     */
    public function categoriesAction(Request $request, $active = "")
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository("AppBundle:Category")->findAll();

        return $this->render('@App/widget_categories.html.twig', array(
            'categories' => $categories,
            'active' => $active
        ));
    }
}